StartPage project

To use...
1. clone the repository (or get the mini-project from me in a zip)
2. keep the directories as they are (dont move any of the files)
3. set the startpage local html file on disk as your new tab and home page
    you will need a chrome plugin to do this for you if you are using Google Chrome
    unsure about other browsers
    for chrome, search for chrome browser extension "new tab redirect"
4. random backgrounds from the backgrounds folder
	to add a background, name it background#.jpg with the # being the next number
	also go into the script.js file and edit the numBackgrounds var to be teh highest number background

Features...
1. Time and date from Javscript global date object
2. Yahoo Weather API call to fetch basic weather and location data
3. quick links on hover
4. search bar with different commands to change the search type
	a. !g = Google
	b. !im = Google Images
	c. !imdb =  IMDB
	d. !wp = Wikipedia
	e. !yt = YouTube
	f. !champion.gg = Champion.gg
	g. !r = Reddit
	h. !op.gg = na.op.gg
	i. !tw = Twitter
5. Notes feature coming soon
	a. saving notes with ctrl+o